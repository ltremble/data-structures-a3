# Data Structures A2

## A2 coding project for data structures

[GitLab Repo](https://git.socs.uoguelph.ca/ltremble/data-structures-a3)

### Author

Luke Tremble

ID: 1167443

2022-11-12

## To compile and Run

Compilation:

```
make
```

Output:

```
gcc -Wall -std=c99 -pedantic -c BinaryTree.c
gcc -Wall -std=c99 -pedantic BinaryTree.o -o bin/BinaryTree
```

To run the program:

```
./bin/BinaryTree '[expression]'
```

Example Output:

```
$ ./bin/BinaryTree '(((x1+5.12)*(x2-7.68))/x3)'
How would you like to output this expression?
1: Preorder
2: Postorder
3: Exit program
1
/ * + x1 5.12 - x2 7.68 x3 
How would you like to output this expression?
1: Preorder
2: Postorder
3: Exit program
2
x1 5.12 + x2 7.68 - * x3 / 
How would you like to output this expression?
1: Preorder
2: Postorder
3: Exit program
3
skywalkertrem@DESKTOP-Q8G6IRB:/mnt/c/Users/skywa/Desktop/DataStructuresCode/data-structures-a3$ 
```