flags := -Wall -std=c99 -pedantic

bin/BinaryTree: BinaryTree.o
	gcc $(flags) BinaryTree.o -o bin/BinaryTree

BinaryTree.o: BinaryTree.c
	gcc $(flags) -c BinaryTree.c

clean:
	rm *.o bin/*