#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

typedef struct BinaryTree
{
    char data[6];
    struct BinaryTree *left;
    struct BinaryTree *right;

}binaryTree;

int countChars (char *input, char delim);

char *findLeftOp(char* input);

char *findRightOp(char* input);

char findOperator(char *input);

binaryTree *createTree(char *input);

void printTreePreorder(binaryTree *leaf);

void printTreePostorder(binaryTree *leaf);
