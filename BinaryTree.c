#include "BinaryTree.h"

int main(int argc, char *argv[])
{
    binaryTree *head;
    head = createTree(argv[1]);


    int menu = 0;
    while (menu != 3)
    {
        printf("How would you like to output this expression?\n");
        printf("1: Preorder\n");
        printf("2: Postorder\n");
        printf("3: Exit program\n");
        scanf("%d", &menu);
        switch (menu)
        {
        case 1:
            printTreePreorder(head);
            printf("\n");
            break;
        
        case 2:
            printTreePostorder(head);
            printf("\n");
            break;

        case 3:
            break;

        default:
            printf("Please enter a number from 1-3.\n");
            break;
        }
    }
    return 0;
}

/**
 * It counts the number of times a character appears in a string
 *
 * @param input The string to be parsed.
 * @param delim The delimiter to count.
 *
 * @return The number of times the delimiter appears in the string.
 */
int countChars(char *input, char delim)
{
    int count = 0;
    for (int i = 0; i < strlen(input); i++)
    {
        if (input[i] == delim)
        {
            count++;
        }
    }
    return count;
}

/**
 * It finds the left operand of a given input.
 * 
 * @param input The string that is being parsed
 * 
 * @return The left operand of the input.
 */
char *findLeftOp(char *input)
{
    if (countChars(input, '(') != 0)
    {
        char *op = malloc(sizeof(char[strlen(input)]));
        if (input[1] == '(')
        {
            int i = 1;
            int count = 0;
            do
            {
                op[i - 1] = input[i];
                // printf("'%s','%c'\n", op, op[i]);
                if (input[i] == '(')
                {
                    count++;
                }
                else if (input[i] == ')')
                {
                    count--;
                }
                i++;
            } while (count > 0);

            return op;
        }
        else if (input[1] == 'x')
        {
            op[0] = 'x';
            op[1] = input[2];
            return op;
        }
        else if (isdigit(input[1]))
        {
            op[0] = input[1];
            op[1] = input[2];
            op[2] = input[3];
            op[3] = input[4];
            return op;
        }
        else
        {
            return "";
        }
    }
    else
    {
        return "";
    }
}

/**
 * It takes a string and returns the right operand in the string
 *
 * @param input a parenthasized inorder string
 *
 * @return The right operand of the input string.
 */
char *findRightOp(char *input)
{
    if (countChars(input, '(') != 0)
    {
        char *op = malloc(sizeof(char[strlen(input)]));
        if (input[strlen(input) - 2] == ')')
        {
            int i = strlen(input) - 2;
            int count = 0;
            do
            {
                op[(strlen(input) - 2) - i] = input[i];
                if (input[i] == ')')
                {
                    count++;
                }
                else if (input[i] == '(')
                {
                    count--;
                }
                i--;
            } while (count > 0);
            char *temp = malloc(sizeof(char[strlen(input)]));
            strcpy(temp, op);
            // op is reversed so we need to reverse it again
            for (int j = strlen(op) - 1; j >= 0; j--)
            {
                op[(strlen(op) - 1) - j] = temp[j];
            }
            free(temp);
            return op;
        }
        else if (isdigit(input[strlen(input) - 2]))
        {
            if (input[strlen(input) - 3] == 'x')
            {
                op[0] = 'x';
                op[1] = input[strlen(input) - 2];
                return op;
            }
            else
            {
                op[0] = input[strlen(input) - 5];
                op[1] = input[strlen(input) - 4];
                op[2] = input[strlen(input) - 3];
                op[3] = input[strlen(input) - 2];
                return op;
            }
        }
        else
        {
            return "";
        }
    }
    else
    {
        return "";
    }
}

/**
 * It finds the operator in the input string by identifying nested parenthasies.
 * 
 * @param input The string that is being evaluated.
 * 
 * @return The operator that is being used in the equation.
 */
char findOperator(char *input)
{
    if (countChars(input, '(') != 0)
    {
        if (input[1] == 'x')
        {
            return input[3];
        }
        else if (isdigit(input[1]))
        {
            return input[5];
        }
        else if (countChars(input, '(') > 1)
        {
            int i = 1;
            int count = 0;
            do
            {
                if (input[i] == '(')
                {
                    count++;
                }
                else if (input[i] == ')')
                {
                    count--;
                }
                i++;
            } while (count > 0);
            return input[i];
        }
        else
        {
            return '\0';
        }
    }
    else
    {
        return '\0';
    }
}

/**
 * It takes a string, finds the operator, finds the left and right operands, creates a new node, and
 * then recursively calls itself to create the left and right branches
 * 
 * @param input a string containing a parenthasized inorder operation
 * 
 * @return A pointer to a binaryTree.
 */
binaryTree *createTree(char *input)
{
    // printf("%s\n", input);
    char operator= findOperator(input);
    // char *lOperand = malloc(sizeof(char[strlen(input)]));
    char *lOperand = findLeftOp(input);
    // char *rOperand = malloc(sizeof(char[strlen(input)]));
    char *rOperand = findRightOp(input);

    binaryTree *branch = malloc(sizeof(binaryTree));
    *branch = (binaryTree){"", NULL, NULL};
    branch->data[0] = operator;
    // printf("op:'%c' lop '%s' rop '%s' ", operator, lOperand, rOperand);
    if (operator== '\0' && lOperand[0] == '\0' && rOperand[0] == '\0')
    {
        strcpy(branch->data, input);
        // printf("1'%s\n'", branch->data);
        return branch;
    }
    if (branch->left == NULL)
    {
        // printf("2'%s'\n", lOperand);
        branch->left = createTree(lOperand);
    }
    if (branch->right == NULL)
    {
        // printf("3'%s'\n", rOperand);
        branch->right = createTree(rOperand);
    }
    return branch;
}

/**
 * recursively print the binary expression tree in a postorder manner
 * 
 * @param leaf the node to start printing from
 */
void printTreePreorder(binaryTree *leaf)
{
    if (leaf->data[0] != '\0')
    {
        printf("%s ", leaf->data);
    }
    if (leaf->left != NULL)
    {
        printTreePreorder(leaf->left);
    }
    if (leaf->right != NULL)
    {
        printTreePreorder(leaf->right);
    }
}

/**
 * recursively print the binary expression tree in a postorder manner
 * 
 * @param leaf the node to start printing from
 */
void printTreePostorder(binaryTree *leaf)
{
    if (leaf->left != NULL)
    {
        printTreePostorder(leaf->left);
    }
    if (leaf->right != NULL)
    {
        printTreePostorder(leaf->right);
    }
    if (leaf->data[0] != '\0')
    {
        printf("%s ", leaf->data);
    }
}
